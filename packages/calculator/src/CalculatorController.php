<?php

namespace Muradbdinfo\Calculator;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
class CalculatorController extends Controller
{

public function index()
{
    $output = DB::table('calculators')->get();
    return view('calculator::index',compact('output'));
}


    // public function add($a, $b){
    // 	// echo $a + $b;
    //     $result = $a + $b;
	// return view('calculator::add', compact('result'));
    // }

public function add()
{
    return view('calculator::add');
}

public function insert(Request $request)
{
   
    $data = array();
    $data['input1'] = $request->input1;
    $data['input2'] = $request->input2;
    $data['results'] = $request->input1+$request->input2;
    
   
    $insert = DB::table('calculators')->insert($data);
    if ($insert) {
             $notification=array(
             'messege'=>'Successfully Calculator Inserted ',
             'alert-type'=>'success'
              );
            return Redirect()->route('index')->with($notification);                      
         }else{
          $notification=array(
             'messege'=>'error ',
             'alert-type'=>'error'
              );
             return Redirect()->route('index')->with($notification);
         }

}


public function update(Request $request)
{
   
    $data = array();
    $data['input1'] = $request->input1;
    $data['input2'] = $request->input2;
    $data['results'] = $request->input1+$request->input2;
    
   
    $insert = DB::table('calculators')->update($data);
    if ($insert) {
             $notification=array(
             'messege'=>'Successfully Calculator Updated ',
             'alert-type'=>'success'
              );
            return Redirect()->route('index')->with($notification);                      
         }else{
          $notification=array(
             'messege'=>'error ',
             'alert-type'=>'error'
              );
             return Redirect()->route('index')->with($notification);
         }

}


public function Edit($id)
{
    $edit=DB::table('calculators')
         ->where('id',$id)
         ->first();
         return view('calculator::edit',compact('edit'));   
}


public function delete($id)
    {
     
        $delete = DB::table('calculators')->where('id', $id)->delete();
        if ($delete)
                            {
                            $notification=array(
                            'messege'=>'Successfully Calculator Deleted ',
                            'alert-type'=>'success'
                            );
                            return Redirect()->back()->with($notification);                      
                            }
             else
                  {
                  $notification=array(
                  'messege'=>'error ',
                  'alert-type'=>'error'
                  );
                  return Redirect()->back()->with($notification);

                  }

      }

    public function subtract($a, $b){
    	// echo $a - $b;
        $result = $a - $b;
        return view('calculator::subtraction', compact('result'));
    }
}
