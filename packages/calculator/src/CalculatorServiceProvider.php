<?php

namespace Muradbdinfo\Calculator;

use Illuminate\Support\ServiceProvider;

class CalculatorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Muradbdinfo\Calculator\CalculatorController');
        $this->loadViewsFrom(__DIR__.'/views', 'calculator');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // include __DIR__.'/routes.php';
        $this->loadRoutesFrom(__DIR__.'./routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'./database/migrations');
    }
}
