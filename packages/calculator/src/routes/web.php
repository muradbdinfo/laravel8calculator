<?php

Route::get('calculator', function(){
	echo 'Welcome to Basic Calculator Packages.';
});

Route::get('/index', 'Muradbdinfo\Calculator\CalculatorController@index')->name('index');
Route::get('/add', 'Muradbdinfo\Calculator\CalculatorController@add');
Route::post('/insert', 'Muradbdinfo\Calculator\CalculatorController@insert');
Route::get('/edit/{id}', 'Muradbdinfo\Calculator\CalculatorController@edit');
Route::post('/update/{id}', 'Muradbdinfo\Calculator\CalculatorController@update');
Route::get('delete/{id}', 'Muradbdinfo\Calculator\CalculatorController@delete');