<table id="example1" class="table table-bordered table-striped">
  <thead>
  <tr>
  <th>ID</th>
  <th>Input1</th>
  <th> Input2 </th>                  
  <th>Output</th>
  <th>Action</th>
  </tr>
  </thead>
                <tbody>
                	 @foreach($output as $row)
                <tr>
                	<td>{{ $row->id }}</td>
                  <td>{{ $row->input1 }}</td>
                  <td>{{ $row->input2 }}</td>
                  <td>{{ $row->results }}</td>
                  </td>
                  
                  <td>

<a href="{{ URL::to('/edit/'.$row->id) }}" class="btn btn-sm btn-info">Edit<i class="fas fa-pencil-alt"></i></a>
<a href="{{ URL::to('delete/'.$row->id) }}" class="btn btn-sm btn-danger" id="delete" class="middle-align"> Delete
<i class="fa fa-trash" aria-hidden="true"></i>
</a>
                                            </td>
                </tr>
                 @endforeach
                
                </tbody>
<tfoot>
<tr>
  <th>ID</th>
  <th>Input1</th>
  <th> Input2 </th>                  
  <th>Output</th>
  <th>Action</th>
  </tr>
</tfoot>
              </table>